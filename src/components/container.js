const Container = props => (
  <div style={{
    border: props.border || '1px solid #eee',
    margin: props.margin || 20,
    width: props.width,
    padding: props.padding,
    ...props.style
  }}>
    {props.children}
  </div>
)

export default Container
