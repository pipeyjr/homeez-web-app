export function assign () {
  return Object.assign.apply(this, arguments)
}

export const uuid = () => {
  let u = ''
  let m = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
  let i = 0
  let rb = Math.random() * 0xffffffff | 0
  while (i++ < 36) {
    let c = m[i - 1]
    let r = rb & 0xf
    let v = c === 'x' ? r : (r & (0x3 | 0x8))
    u += (c === '-' || c === '4') ? c : v.toString(16)
    rb = i % 8 === 0 ? Math.random() * 0xffffffff | 0 : rb >> 4
  }
  return u
}
