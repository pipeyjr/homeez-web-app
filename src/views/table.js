import { Layout, Table, Button, Pagination } from 'element-react'
import { useState, useLayoutEffect, useEffect, useRef, useMemo } from 'react'

import useStore from '../context/useStore'

import { QUOTATION } from '../utilities/constants'

import Container from '../components/container'

/** @type {import('../internal').Colums} */
const columns = [
  {
    label: 'Index',
    prop: 'Q_ID',
    width: 80
  },
  {
    label: 'Quotation Info',
    prop: 'Quotation_Info'
  },
  {
    label: 'Quotation Valid',
    prop: 'Quotation_Valid',
    width: 180
  }
]

function TableComponent ({ history }) {
  const { store } = useStore()

  const {
    paginateItems = [],
    total = []
  } = useMemo(() => store.getData(QUOTATION) || {}, [store])

  const [state, setState] = useState({
    columns,
    data: [{}]
  })

  const [count, setCount] = useState(10)

  useEffect(() => {
    if (paginateItems && paginateItems.length) {
      setState(state => ({
        ...state,
        data: paginateItems.map(q => {
          q.Quotation_Valid = q.Quotation_Valid.toString()
          return q
        })
      }))
    }
    if (total && total.length) {
      setCount(total[0].count)
    }
  }, [paginateItems, total])

  const [currentPage, setCurrentPage] = useState(1)

  const firstUpdate = useRef(true)

  useLayoutEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false

      store.dispatch(QUOTATION, { action: 'get', page: 1, rowsPerPage: 10 })
    }

    return () => {
      // store.cancel(QUOTATION)
    }
  }, [store])

  const viewForm = () => {
    history.push('/form')
  }

  const onCurrentChange = e => {
    if (e !== currentPage) {
      setCurrentPage(e)
      store.dispatch(QUOTATION, { action: 'get', page: e, rowsPerPage: 10 })
    }
  }

  return (
    <div>
      <Layout.Row>
        <Layout.Col>
          <Container border='none'>
            <Table
              style={{ width: '100%' }}
              columns={state.columns}
              data={state.data}
            />
            <Container margin='0' padding='7px 0' style={{borderTop: 'none', display: 'flex', flexDirection: 'column', alignItems: 'flex-end'}}>
              <Pagination layout='prev, pager, next' total={count} currentPage={currentPage} small onCurrentChange={onCurrentChange} />
            </Container>
          </Container>
        </Layout.Col>
      </Layout.Row>
      <Layout.Row>
        <Layout.Col>
          <Container border='none' margin='0 20px'>
            <Button type='primary' onClick={viewForm}>View Submission Form</Button>
          </Container>
        </Layout.Col>
      </Layout.Row>
    </div>
  )
}

export default TableComponent
