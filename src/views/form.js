import { Layout, Form, Input, Select, Button } from 'element-react'
import { useState, useEffect } from 'react'

import useStore from '../context/useStore'

import { QUOTATION } from '../utilities/constants'

import Container from '../components/container'

function FormComponent ({ history }) {
  const { store } = useStore()

  const [form, setForm] = useState({
    quotationInfo: '',
    quotationValid: 'true'
  })

  const [date, setDate] = useState('')

  const [log, setLog] = useState('')

  // KeepAlive cache path
  const [noCache, setNoCache] = useState(false)

  useEffect(() => {
    if (form.quotationInfo.length) {
      if (date) {
        setLog(`Submitted Data on ${date}`)
      } else {
        setLog('')
      }
    } else {
      setLog('Quotation Info is empty')
    }
  }, [form, date])

  const onSubmit = e => {
    e.preventDefault()
    if (!form.quotationInfo.length) {
      return
    }
    setNoCache(true)
    store.dispatch(QUOTATION, {
      action: 'set',
      data: {
        ...form,
        quotationValid: JSON.parse(form.quotationValid)
      }
    }, () =>
      setDate(new Date().toLocaleTimeString())
    )
  }

  const onChange = (key, value) => {
    setForm({ ...form, ...{ [key]: value } })
  }

  const viewTable = () => {
    history.push('/table', { noCache })
  }

  return (
    <div>
      <Layout.Row>
        <Layout.Col>
          <Container width={330} padding='20px 20px 0 0'>
            <Form className='en-US' model={form} labelWidth='120' onSubmit={onSubmit}>
              <Form.Item label='Quotation Info'>
                <Input value={form.quotationInfo} onChange={onChange.bind(null, 'quotationInfo')} autoFocus />
              </Form.Item>
              <Form.Item label='Quotation Valid'>
                <Select value={form.quotationValid} onChange={onChange.bind(null, 'quotationValid')}>
                  <Select.Option label='TRUE' value='true' selected />
                  <Select.Option label='FALSE' value='false' />
                </Select>
              </Form.Item>
            </Form>
          </Container>
        </Layout.Col>
      </Layout.Row>
      <Layout.Row>
        <Layout.Col>
          <Container border='none' margin='0 20px'>
            <Button type='primary' onClick={onSubmit}>Submit Quotation</Button>
            <Button type='primary' onClick={viewTable}>View Quotation Table</Button>
          </Container>
          <Container border='none'>
            {log}
          </Container>
        </Layout.Col>
      </Layout.Row>
    </div>
  )
}

export default FormComponent
