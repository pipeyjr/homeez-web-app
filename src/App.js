import Router from './config/routing'
import { Provider } from './context/Provider'

function App () {
  return (
    <Provider>
      <Router />
    </Provider>
  )
}

export default App
