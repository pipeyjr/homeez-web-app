export type Routes = Array<{ path: String, name: String, component: Object }>

export type Colums = Array<{
    label: String,
    prop: String,
    width?: Number
}>