import { useContext } from 'react'
import { StoreContext, ActionContext, PayloadContext } from './Provider'

import {
  USER_LOGIN,
  USER_LOGOUT,
  USER_DATA
} from '../utilities/constants'

const useStore = () => {
  const [store, setStore] = useContext(StoreContext)
  const [action, setAction] = useContext(ActionContext)
  const [payload, setPayload] = useContext(PayloadContext)

  function updateStore (res, act, params) {
    // only diff DOM tree on action other then set
    if (params && params.action !== 'set') {
      Object.keys(res).forEach(key => {
        if (typeof res[key] === 'object' && !Array.isArray(res[key])) {
          setStore(store => ({
            ...store,
            ...{
              [act]: {
                ...store[act],
                [key]: {
                  ...store[act][key],
                  ...res[key]
                }
              }
            }
          }))
        } else {
          setStore(store => ({
            ...store,
            ...{
              [act]: {
                ...store[act],
                [key]: res[key]
              }
            }
          }))
        }
      })

      // cache after request
      if (action[act]._enableCache) {
        setAction(action => ({
          ...action,
          ...{
            [act]: {
              ...action[act],
              _dirty: false
            }
          }
        }))
      }
    }

    setPayload(payload => ({
      ...payload,
      ...{
        [act]: {
          ...payload[act],
          _params: params,
          _timestamp: new Date()
        }
      }
    }))
  }

  function cancel (act, message = 'Canceling inflight request.') {
    const source = action[act].source || {}
    source.cancel && source.cancel(message)
  }

  // register inflight request cancelation
  function unsub (act) {
    return function (source) {
      setAction(action => ({
        ...action,
        ...{
          [act]: {
            ...action[act],
            source
          }
        }
      }))
    }
  }

  // get new data or update new data to store
  function dispatch (act, params = {}, caller) {
    if (action[act]) {
      let request = action[act]._api

      if (typeof request !== 'function') throw new Error('API endpoint not defined')

      if (action[act]._dirty) {
        return request(params, unsub(act)).then(res => {
          if (process.env.NODE_ENV === 'development') { console.log(res) }

          if (act === USER_LOGIN || act === USER_LOGOUT) {
            updateStore(res, USER_DATA, params)
          } else {
            updateStore(res, act, params)
          }
          caller && typeof caller === 'function' && caller(res)

          return res
        })
      } else {
        caller && typeof caller === 'function' && caller(store[act])
        return store[act]
      }
    }
  }

  function getData (act, param) {
    if (param) {
      return (
        (store[act] &&
                    store[act].result &&
                    store[act].result[param]) ||
                null
      )
    } else {
      return (
        (store[act] &&
                    store[act].result) ||
                null
      )
    }
  }

  function getObject (act) {
    return store[act]
  }

  function getStatus (act) {
    return (store[act] && store[act].success) || false
  }

  function getSync (act, param, _default = null) {
    return (store[act] && store[act][param]) || _default
  }

  function resetData (act, param, value) {
    value = value || null
    if (param && store[act] && store[act][param]) {
      setStore(store => ({
        ...store,
        ...{
          [act]: {
            ...store[act],
            [param]: value
          }
        }
      }))
    } else if (act) {
      setStore(store => ({
        ...store,
        ...{
          [act]: {}
        }
      }))
    }
  }

  function setObject (act, val) {
    store[action] = { ...store[act], ...val }
  }

  function comparePayload (oldPayloads = {}, newPayloads = {}) {
    if (Object.keys(oldPayloads).length !== Object.keys(newPayloads).length) {
      return true
    } else {
      const compare = Object.keys(newPayloads).map(payload => {
        if (newPayloads[payload] !== oldPayloads[payload]) {
          return true
        }
        return false
      })
      return !!~compare.indexOf(true)
    }
  }

  function clearCache (act, newPayloads = {}) {
    if (payload[act]) {
      const oldPayloads = payload[act]._params || {}
      const oldTimestamp = payload[act]._timestamp || new Date('1970')

      let isDirty = false

      if (new Date() - oldTimestamp > (1000 * 60 * 15)) {
        isDirty = true
      } else {
        isDirty = comparePayload(oldPayloads, newPayloads)
      }

      if (isDirty) {
        setAction(action => ({
          ...action,
          ...{
            [act]: {
              ...action[act],
              _dirty: true
            }
          }
        }))
      }
    }
  }

  const storeAPI = {
    dispatch,
    cancel,
    getData,
    getObject,
    getStatus,
    getSync,
    resetData,
    setObject,
    clearCache
  }

  if (process.env.NODE_ENV === 'development') { window.store = store }

  return {
    store: storeAPI
  }
}

export default useStore
