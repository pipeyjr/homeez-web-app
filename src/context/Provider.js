import React, { useState } from 'react'

import dataStore from './store/dataStore'
import actionStore from './store/actionStore'

const StoreContext = React.createContext([{}, () => { }])
const ActionContext = React.createContext([{}, () => { }])
const PayloadContext = React.createContext([{}, () => { }])

const Provider = (props) => {
  const [store, setStore] = useState(dataStore)
  const [action, setAction] = useState(actionStore)
  const [payload, setPayload] = useState({})
  return (
    <StoreContext.Provider value={[store, setStore]}>
      <ActionContext.Provider value={[action, setAction]}>
        <PayloadContext.Provider value={[payload, setPayload]}>
          {props.children}
        </PayloadContext.Provider>
      </ActionContext.Provider>
    </StoreContext.Provider>
  )
}

export { Provider, StoreContext, ActionContext, PayloadContext }
