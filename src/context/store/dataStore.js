const dataStore = {
  USER_LOGIN: {},
  USER_LOGOUT: {},
  USER_DATA: {
    success: false,
    result: {
      username: ''
    }
  },
  QUOTATION: {}
}

export default dataStore
