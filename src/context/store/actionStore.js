import { requestLogin,
  requestTerminate,
  verifySession
} from './api/session'

import {
  requestQuotation
} from './api/client'

const actionStore = {
  /**
     * User login
     * `/session?requestLogin=1`
     */
  USER_LOGIN: {
    _api: requestLogin,
    _dirty: true,
    _enableCache: false
  },
  /**
     * User logout
     * `/session?terminate=1`
     */
  USER_LOGOUT: {
    _api: requestTerminate,
    _dirty: true,
    _enableCache: false
  },
  /**
     * User store
     * user data fetch with `/session?verify=1`
     */
  USER_DATA: {
    _api: verifySession,
    _dirty: true,
    _enableCache: true
  },
  QUOTATION: {
    _api: requestQuotation,
    _dirty: true,
    _enableCache: false
  }
}

export default actionStore
