import axios from 'axios'
import { assign } from '../../../utilities/helpers'

import { api, apiKey } from '../../../config'

const devel = process.env.NODE_ENV === 'development'

let base = {
  timeout: 30000,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
    'X-Api-Key': apiKey
  }
}

const CONFIG_DEVEL = assign({}, base, { baseURL: 'http://localhost:5000' })

const CONFIG_PROD = assign({}, base, { baseURL: api })

const conf = (devel && CONFIG_DEVEL) || CONFIG_PROD

const Api = (endpoint, data, timeout, inflightRequestCancelation) => {
  if (timeout) {
    conf['timeout'] = timeout
  }

  const api = axios.create(conf)

  const source = axios.CancelToken.source()

  if (inflightRequestCancelation && typeof inflightRequestCancelation === 'function') {
    inflightRequestCancelation(source)
  }

  let reqData = {}
  if (data) {
    reqData = {
      ...reqData,
      ...data,
      cancelToken: source.token
    }
  }
  reqData = JSON.stringify(reqData)
  return api
    .post(endpoint, reqData)
    .then(res => res.data)
    .catch(err => {
      console.log(new Error(err))
      return {
        success: false,
        message: err.message
      }
    })
}

export default Api
