import request from './'

export const requestLogin = async (params, unsub) => {
  const res = await request('/session?requestLogin=1', params, null, unsub)
  return res
}

export const verifySession = async (params, unsub) => {
  const res = await request('/session?verify=1', params, null, unsub)
  return res
}

export const requestTerminate = async (params, unsub) => {
  const res = await request('/session?terminate=1', params, null, unsub)
  return res
}

export const requestResetPassword = async (params, unsub) => {
  const res = await request('/session?resetPassword=1', params, null, unsub)
  return res
}
