import request from './'

export const requestQuotation = async (params, unsub) => {
  const res = await request('/client?requestQuotation=1', params, null, unsub)
  return res
}
