import KeepAlive, { AliveScope } from 'react-activation'
import { MessageBox } from 'element-react'
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom'

import Table from '../views/table'
import Form from '../views/form'

/** @type {import('../internal').Routes} */
const staticRoutes = [
  {
    path: '/table', name: 'table', component: Table
  },
  {
    path: '/form', name: 'dashboard', component: Form
  }
]

const AppRoute = () => {
  const getUserConfirmation = (message, callback) => {
    MessageBox
      .confirm(message, 'Hello This\'s Confirm Message', { type: 'warning' })
      .then(() => callback(true))
      .catch(() => callback(false))
  }

  return (
    <BrowserRouter getUserConfirmation={getUserConfirmation}>
      <AliveScope>
        <Switch>
          {staticRoutes.map(route => {
            const RouteComponent = route.component
            return (<Route key={route.name} path={route.path} render={
              props => {
                const { location } = props
                return (
                  <KeepAlive when={location.pathname === '/form'}>
                    <RouteComponent {...props} />
                  </KeepAlive>
                )
              }
            } />)
          })}
          <Redirect from='/' to='/form' />
        </Switch>
      </AliveScope>
    </BrowserRouter>
  )
}

export default AppRoute
